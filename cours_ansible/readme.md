# Ansible

## Installation

    sudo yum install ansible

    ansible --version
    ansible 2.7.5
        config file = /etc/ansible/ansible.cfg
        configured module search path = [u'/home/alex/.ansible/plugins/modules', u'/usr/share/ansible/plugins/modules']
        ansible python module location = /usr/lib/python2.7/site-packages/ansible
        executable location = /usr/bin/ansible
        python version = 2.7.5 (default, Oct 30 2018, 23:45:53) [GCC 4.8.5 20150623 (Red Hat 4.8.5-36)]

## Use
Add machine to inventory:

    $cat rec-apache.inv 
    vps196233.vps.ovh.ca ansible_user=alex

Ping all machines in the inventory:

    $ansible -i rec-apache.inv -m ping all
    vps196233.vps.ovh.ca | SUCCESS => {
        "changed": false, 
        "ping": "pong"
    }

View setup of all machines:
    
    $ansible -i rec-apache.inv -m setup -a 'filter=ansible_user_id' all

# CMDB
https://computingforgeeks.com/how-to-generate-host-overview-from-ansible-fact-gathering-output-using-ansible-cmd/

    ```
    ansible -i test.inv -m setup --tree cmdb-output/ all
    ```

# Use molecule to test your ansible roles :

[Get started](https://developer.rackspace.com/blog/molecule-for-ansible-role-creation/)