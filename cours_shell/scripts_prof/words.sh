#!/bin/bash
if [ $# -ne 1 ]; then
echo "more param than expected"
echo "usage: words.sh <fichier>"
exit 1
fi

liste_unique=$(cat $1 | sed "s/[[:punct:]]/\\n/g" | sed "s/[[:space:]]/\\n/g" | sed "/^$/d" | sort | uniq)
liste=$(cat $1 | sed "s/[[:punct:]]/\\n/g" | sed "s/[[:space:]]/\\n/g" | sed "/^$/d")
nb=$(echo $liste | wc -w)
nb_u=$(echo $liste_unique | wc -w)
echo "nombre de mots dans ce texte : " $nb
echo "nombre de mots differents dans ce texte : " $nb_u
echo $liste
echo $uniq_liste

