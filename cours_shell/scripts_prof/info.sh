#!/bin/bash

fich=$1
if [ -s $fich ]; then
type=$(ls -l $(dirname $fich)| grep $fich | cut -c1)
droit=$(ls -l $(dirname $fich) | grep $fich |cut -c2,3,4 |sed 's/-//g')
fi
case $type in 
-) echo "$fich est un fichier ($droit)";;
d) echo "$fich est un repertoire ($droit)";;
l) echo "$fich est un lien ($droit)";;
*) echo "$fich n'existe pas";;
esac
