#!/bin/bash
if [ $# -ne 1 ]; then
echo "more argument than expected!"
echo "usage: nospace.sh <file_name>"
exit 1
fi
curr_nom="$1"
if [ -f "$curr_nom" ]; then 
echo "ceci est un fichier"
elif [ -d "$curr_nom" ]; then 
echo "ceci est un repertoire"
else
echo "le fichier  est introuvable"
exit 2
fi

test=$(echo "$curr_nom" | grep ' ')
if [ -z "$test" ]; then
	echo "pas d espace trouvé"
else
	nom=$(echo "$curr_nom" | sed "s/ /_/g")
	echo "$curr_nom>>$nom"
	mv "$curr_nom" $nom
fi
