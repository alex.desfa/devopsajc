#!/bin/bash
echo "nom du programme : " `basename $0`
echo "Nb d'argument :" $#
echo "Tous les argument : " $*
echo "X Source : " $1
echo "Cible : " $2
if [ -f $1 ]; then
	cp $1 $2
elif [ -d $1 ]; then
	cp -r $1 $2
else
	echo "copie impossible! vérifiez que la source existe"
fi

