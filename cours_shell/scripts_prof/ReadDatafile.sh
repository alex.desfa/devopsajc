#!/bin/bash
Old_IFS=$IFS
IFS=$'\n'

File=$1
echo "-----------------------"
echo "premiere methode"
echo "----------"
while read line 
do
	echo $line
	IFS=$';'
done <$File
echo "-----------------------"
echo "Deuxieme methode"
echo "--------"
IFS=$'\n'
for line in $(cat $File)
do
	echo "****************"
	echo "**** ${line} *****"
	IFS=$';'
	for val in $line
	do
		echo $val
	done
done

