#!/bin/bash
echo "démarrage de la copie de $1">copie2fois.log
echo "source : " $1 >>copie2fois.log
echo "le fichier sera copié en double avec les noms $2 et $3">>copie2fois.log
if [ -f $1 ]; then
	cp $1 $2
	cp $1 $3
else
	echo "copie impossible! vérifiez que la source existe" &>> copie2fois.log
fi

