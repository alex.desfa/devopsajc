#!/bin/bash

TRASH_DIR="$HOME/trash"

usage()
{
    echo "Usage: $0 <files> ... move files in ~/trash"
    echo "Usage: $0 -f flush ~/trash"
}

printerr()
{
    echo "Error: $*" >&2
}

move_to_trash()
{
    mv "$1" "$TRASH_DIR"
    if [ $? -eq 0 ];then
        echo "$1 moved to trash"
    else
        printerr "$1 failed to be trashed"
    fi
}

create_trash_dir()
{
    if [ ! -d "$TRASH_DIR" ];then
        mkdir -p "$TRASH_DIR"
    fi
}

flush_trash()
{
    rm -rf "$TRASH_DIR"
    if [ $? -eq 0 ];then
        echo "trash flushed"
    else
        printerr "failed to flush trash"
    fi
}

trash_it()
{
    file="$1"
    if [ -f "$file" -o -L "$file" ];then
        move_to_trash "$file"
    else
        printerr "No file named $file"
    fi
}

if [ $# -eq 0 ]; then
    printerr "No arguments given"
    usage
    exit 1
elif [ $# -eq 1 ] ; then
    case $1 in
        -h | --help)
            usage
            ;;
        -f | --flush)
            flush_trash
            ;;
        *)
            create_trash_dir
            trash_it "$1"
            ;;
    esac
else
    create_trash_dir
    while [ $# -gt 0 ];do
        trash_it "$1"
        shift
    done
fi
