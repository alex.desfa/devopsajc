#!/bin/bash

usage()
{
    echo "Usage: $0 save all .txt files in ~/backup"
}

printerr()
{
    echo "Error: $*" >&2
}

save_txt_files()
{
    find $HOME -path $HOME/backup -prune -o -type f -name "*.txt" -exec cp -n -v {} $HOME/backup \;
}

create_backup_dir()
{
    if [ ! -d $HOME/backup ];then
        mkdir -p $HOME/backup
    fi
}

if [ $# -ne 0 ]; then
    printerr "No arguments needed"
    exit 1
fi 

create_backup_dir
save_txt_files
