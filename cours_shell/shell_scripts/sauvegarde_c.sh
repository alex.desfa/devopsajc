#!/bin/bash

BACKUP_DIR="$HOME/backup_c"

usage()
{
    echo "Usage: $0 [ext] save all .c files in ~/backup_c with the extension given"
}

printerr()
{
    echo "Error: $*" >&2
}

save__files()
{
    ext=$1
    list=$(find $HOME -path "$BACKUP_DIR" -prune -o -type f -name "*.c")
    for file in $list; do
        newfile=$BACKUP_DIR/$(basename "$file").$ext
        cp $file $newfile
        if [ $? -eq 0 ];then
            echo "Copy $file to $newfile"
        else
            printerr "copy of $file to $newfile failed"
        fi
    done
}

create_backup_dir()
{
    if [ ! -d $BACKUP_DIR ];then
        mkdir -p $BACKUP_DIR
    fi
}

if [ $# -ne 1 ]; then
    printerr "One argument needed"
    usage
    exit 1
fi 

create_backup_dir
save__files $1
