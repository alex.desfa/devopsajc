#!/bin/bash

usage()
{
    echo "usage : $0 [n] print the first 'n' numbers of the Fibonnacci suite" 
    echo "usage : $0 [n] [file] print the first 'n' numbers of the Fibonnacci suite in file" 
}

affiche_fibo()
{
    res=0
    n1=0
    n2=1
    for (( i=0; i<$1;i++)); do
       echo -n "$n1 "
       if [ $# -eq 2 ];then echo "$n1">>$2;fi
       #(( res = n1 + n2 ))
       res=$(echo "$n1 + $n2" | bc)
       n1=$n2
       n2=$res
    done
    echo "." 
}

affiche_fibo_rec()
{
    case $1 in
        0)
            echo "Error" && exit 2;;
        "1")
            echo 0;;
        "2")
            echo 1;;
        *)
            f1=$(affiche_fibo_rec $(($1-1)))
            f2=$(affiche_fibo_rec $(($1-2)))
            echo "$f1 + $f2" | bc
            ;;
    esac
}

create_file()
{
    echo "The Fibo suite is :" > $1
}


if [ $# -lt 1 -o $# -gt 2 ];then
    echo "Error: one or two argument[s] needed ($# given)" 
    usage
    exit 1
else
    if [ $1 -eq 0 ]; then
        echo "Error : bad argument"
        exit 2
    else
        if [ $# -eq 1 ];then
            affiche_fibo $1
            #affiche_fibo_rec $1
        else
            create_file $2
            affiche_fibo $1 $2
        fi
    fi
fi
