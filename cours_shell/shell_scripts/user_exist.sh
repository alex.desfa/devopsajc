#!/bin/bash

if [ ! $# -eq 1 ]; then
	echo "Error : only 1 argument needed"
	exit 1
fi

username=$1
errorcode=0
grep -c ^$username /etc/passwd &>/dev/null
if [ $? -eq 0 ]; then
	cuttedname=$(grep ^$username /etc/passwd 2>/dev/null | cut -d : -f 1)
	if [ $cuttedname = $username ];then
		echo "User named $username exists on this host"
	else
		echo "No user named $username on this host"
		errorcode=1
	fi
else
	echo "No user named $username on this host"
	errorcode=1
fi
exit $errorcode
