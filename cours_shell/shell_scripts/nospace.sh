#!/bin/bash
if [ ! $# -eq 1 ];then
        echo "Error : 1 arg only"
        exit 1
fi

printerr()
{
    echo "Error: $*" >&2
}

myfile="$1"
if [ ! -e "$myfile" ]; then
    printerr "$myfile does not exit"
    exit 1
else
    newfile=$(echo "$myfile" | sed 's/ /_/g')
    if [ "$newfile" != "$myfile" ]; then
        echo "Renaming \"$myfile\" to \"$newfile\""
        mv "$myfile" "$newfile"
    else
        printerr "$myfile does not need to be renamed"
        exit 1
    fi

fi    

