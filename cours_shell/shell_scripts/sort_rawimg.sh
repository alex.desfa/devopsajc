#!/bin/bash

LOG_FILE="./sort_file.log"

usage()
{
    echo -e "\nInfo:"
    echo -e "\t save all .ext files in a /ext directory whenever files with the .ext extension are found in a given directory"
    echo -e "\nUsage:\n\t $0 [OPTIONS] <ext,ext1,ext2> <directory> <directory2> ...\n"
    echo -e "\n OPTIONS are:\n"
    echo -e "\t -h | --help \t print this help"
    echo -e "\t -r | --recursive \t recursive into <directory>"
    echo -e "\t -l | --local \t do note recurse into <directory>"
    echo -e "\n EXAMPLES:\n"
    echo -e "\t ./sort_rawimg.sh -r cr2 ./test"
    echo -e "\n"
}

printerr()
{
    echo "Error: $*" >&2
}

printinfo()
{
    echo -e "Info: $*" >&1
}

log()
{
    echo -e "Log: $*" >>$LOG_FILE
}

smart_move()
{
  #1 source, #2 dest dir, #3 extension
  echo "$2" | grep -q "$3/$3"
  if [ $? -gt 0 ]; then
    if [ ! -d $2 ]; then
      log "creating subdir $2"
      mkdir "$2"
    fi
    log "moving $1 file into $2"
    mv --backup=numbered "$1" "$2" 2>>$LOG_FILE
    return $?
  else
    log "file $1 seems to be already sorted.nothing done"
    return 1
  fi
}

sort_files()
{
    printinfo "sorting dir: $1" && log "sorting dir: $1"
    if [ $resursive_mode -eq 0 ];then FIND_OPT="-maxdepth 1";else FIND_OPT="";fi

    for ext in $extensions_list; do
      #moving files
      count=0
      for file in $(find "$1" $FIND_OPT -type f -name "*.$ext" 2>/dev/null); do
        destdir="$(dirname ${file})/$ext"
        printinfo "smart move ${file} to ${destdir}"
        smart_move "${file}" "${destdir}" $ext
        if [ $? -eq 0 ]; then
          (( count = count + 1 ))
        fi
      done
      printinfo "$count .$ext files were moved in directory $1"

    done
}
# global OPTIONS
resursive_mode=0
extensions_list=""

if [ $# -lt 2 ]; then
    printerr "Not enough arguments given"
    usage
    exit 1
else
    # parse OPTIONS
    case $1 in
        -h | --help) usage && exit 0 ;;
        -r | --recursive) resursive_mode=1 ;;
        -l | --local) resursive_mode=0 ;;
        *)  printerr "Bad argument given"
            usage
            exit 1
            ;;
    esac
    shift

    #parse extensions_list
    extensions_list=$(echo "$1" | sed 's/,/ /g')
    printinfo "will search for and sort following extensions:\n\t$extensions_list"
    shift

    #parse directories
    while [ $# -gt 0 ]; do
      #do the job
      sort_files "$1"
      shift
    done
fi
