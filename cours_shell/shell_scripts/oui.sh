#!/bin/bash

read -n 1 -p "Oui ou Non [oO/nN] ? " entry
echo ""
if [ $entry = "o" -o $entry = "O" ]; then
      	echo "OUI"
elif [ $entry = "n" -o $entry = "N" ]; then
	echo "NON"
else
	echo "Réponse incorrecte"
fi

while true
do
   read -n 1 -p "Oui ou Non [oO/nN] ? " entry
   echo ""
   case $entry in
	   o | O)
		echo "OUI";;
	   n | N)	
		echo "NON";;
	   *)
		echo "Réponse incorrecte";;
   esac
done
