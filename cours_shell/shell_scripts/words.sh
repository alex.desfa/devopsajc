#!/bin/bash
if [ ! $# -eq 1 ];then
    echo "Error : 1 argument needed only"
    exit 1
fi

printerr()
{
    echo "Error: $*" >&2
}
myfile="$1"
if [ ! -e "$myfile" ]; then
    printerr "$myfile does not exit"
    exit 1
else
    lastword=""
    wordcount=0
    totalwords=0
    totaldifferentwords=0
    #tr -s [:punc:] "\n" equivalent à sed 's/[[:punct:]]//n/g'
    list=$(cat $myfile | tr -s [:punct:] "\n" | sed 's/[[:space:]]/\n/g' | grep -v ^$ | sort -r)
    uniqlist=$(cat $myfile | tr -s [:punct:] "\n" | sed 's/[[:space:]]/\n/g' | grep -v ^$ | sort -r | uniq -i)
    echo "***$uniqList***"
    echo "Total words: $(echo $list | wc -w)"
    echo "Total unique words: $(echo $uniqlist | wc -w)"

    list=$list" END"
    for word in $list
    do
        word=$(echo $word | tr '[:upper:]' '[:lower:]')
        (( totalwords = totalwords + 1 ))
        if [ "$word" != "$lastword" ];then
            if [ -n "$lastword" ];then
               echo "$lastword : $wordcount"
            fi
            (( totaldifferentwords = totaldifferentwords + 1 ))
            wordcount=1
        else
            (( wordcount = wordcount + 1 ))
        fi

        # print output
        lastword=$word
    done
    (( totalwords = totalwords - 1 ))
    (( totaldifferentwords = totaldifferentwords - 1 ))
    echo "Nombre de mots dans ce texte $totalwords"
    echo "Nombre de mots differents dans ce texte $totaldifferentwords"

#    echo "... uniq command inversed return :"
#    cat $myfile | tr -s [:punct:] "\n" | sed 's/[ \t]/\n/g' | grep -v ^$ | sort | uniq -c |  sed -e 's/^.*\([0-9]\)\(.[a-z]*\)/\2 \1/g'
fi
