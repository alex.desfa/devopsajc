#!/bin/bash

if [ ! $# -eq 1 ];then
	echo "Error : 1 arg only"
	exit 1
fi
myfile=$1
if [ ! -f $myfile -a ! -d $myfile ]; then
	echo "$myfile n'existe pas"
else
  #droits=$(ls -l $myfile | cut -c 2,3)
  [ -r $myfile ] && readable="r" || readable="-"
  [ -w $myfile ] && writable="w" || writable="-"
  droits="$readable$writable"
  if [ -f $myfile  -a ! -L $myfile ];then
	  echo "$myfile est un fichier ($droits)"
  elif [ -d $myfile -a ! -L $myfile ];then
	  echo "$myfile est un repertoire ($droits)"
  elif [ -L $myfile ];then
	echo "$myfile est un lien"
  else
    echo "$myfile est d'un type inconnu"
  fi
fi
