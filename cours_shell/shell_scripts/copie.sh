#!/bin/bash

echo "Nom du programme:$0, Nombre d'arguments:$#"
echo "Tous les arguments"
for argument in $*;
do
	echo $argument
done
source=$1
destination=$2
echo "Source=$source"
echo "Destination=$destination"
if [ -f $source ];then
	cp $source $destination
elif [ -d $source ]; then
	cp -r $source $destination
else
	echo "Error: no source file"
	exit 1
fi
