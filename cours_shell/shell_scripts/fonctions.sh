#!/bin/bash

usage()
{
    echo "Usage: $0 "
    echo "       $0 "
}
    
printerr()
{
    echo "Error: $*" >&2
}

test_file()
{
     if [ ! -e "$1" ]; then
         printerr "$1 does not exit"
         exit 1
     fi
}

sum_file()
{
    sum=0
    for nb in $(cat $1);do
        (( sum = sum + nb ))
    done
    echo "Sum is $sum"

}

nb_ligne()
{
    echo "Number of line is $(cat $1 | wc -l) in $1"
}

min_file()
{
    min=$(head -n 1 $1)
    for nb in $(cat $1);do
        if [ $nb -lt $min ];then
            min=$nb
        fi
    done
    echo "Minimum is $min"
}

max_file()
{
    max=0
    for nb in $(cat $1);do
        if [ $nb -gt $max ];then
            max=$nb
        fi
    done
    echo "Maximum is $max"
}


if [ $# -ne 2 ];then
    echo "Error : 2 arguments needed ($# given)"
    usage
    exit 1
fi

if [ -f $1 ];then
    case $2 in
        -s | --somme)
            sum_file $1
            ;;
        -n | --nbligne)
            nb_ligne $1
            ;;
        -min | --minimum)
            min_file $1
            ;;
        -max | --maximum)
            max_file $1
            ;;
        *)
            usage
            exit 1
            ;;
    esac
else
    printerr "no file named $1"
    usage
    exit 1
fi



