#!/bin/bash

echo "Nom du programme:$0, Nombre d'arguments:$#"
source=$1
dest1=$2
dest2=$3

echo "Source=$source"
echo "Destination 1=$dest1"
echo "Destination 2=$dest2"
if [ -e $source ];
then
	cp $source $dest1

	cp $source $dest2

else
	echo "Error: no source file"
	exit 1
fi
