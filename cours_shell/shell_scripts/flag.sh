#!/bin/bash

usage()
{
    echo "Usage: $0 <nom_du_drapeau> retourne l'etat du flag"
    echo "       $0 <nom_du_drapeau> [on/off/flop] positionne ou inverse le drapeau"
}
    
printerr()
{
    echo "Error: $*" >&2
}

create_file()
{
    if [ ! -e "$myflag" ]; then
        touch $myflag
    fi
}

test_file()
{
     if [ ! -e "$myflag" ]; then
         printerr "$myflag does not exit"
         exit 1
     fi
}

if [ $# -lt 1 -o $# -gt 2 ];then
    echo "Error : 1 or 2 arguments needed ($# given)"
    usage
    exit 1
fi

myflag="$1"

if [ $# -gt 1 ];then
    case $2 in
        on)
            create_file
            echo 1 > $myflag
            ;;
        off)
            create_file
            echo 0 > $myflag
            ;;
        flop)
            test_file
            val=$(cat $myflag)
            newval="0"
            if [ $val = "0" ];then newval="1";else newval="0";fi
            echo $newval > $myflag
            ;;
        *)
            usage
            ;;
    esac
else
    #print current state
    test_file
    echo "$(cat $myflag)"
fi



