# Jenkins

## Native installation on Centos7

    https://linuxize.com/post/how-to-install-jenkins-on-centos-7/

## Installation in Docker
1. Follow the instructions from :

    https://github.com/jenkinsci/docker/blob/master/README.md

    and run it 
    ```bash
    docker run -d -p 8080:8080 -p 50000:50000 -v jenkins_home:/var/jenkins_home jenkins/jenkins:lts
    ```

2. Do initial setup for `admin` user:
    ```bash
    Jenkins initial setup is required. An admin user has been created and a password generated.
    Please use the following password to proceed to installation:

    0e274c2b8808412eb53cf5782aa73b4d

    This may also be found at: /var/jenkins_home/secrets/initialAdminPassword
    ```
## Use of Pipelines
    
    node {
        stage('git'){
            git branch: 'develop', credentialsId: 'gitlabimago', url: 'http://vps196185.vps.ovh.ca/alexandre/ajc-jenkins.git'
        }
        stage('build'){
            withMaven(jdk: 'java-sdk8', maven: 'mvn-3.1.1', tempBinDir: '') {
                sh "mvn clean verify"
            }
            step([$class: 'JUnitResultArchiver', testResults: 'target/surefire-reports/*.xml'])
            step([$class: 'JUnitResultArchiver', testResults: 'target/failsafe-reports/*.xml'])
            dir('target') { archive '*.jar' }
            stash name : 'binary', includes : 'target/*.jar'
        }
        stage('Build Docker img') {
            unstash 'binary'
            
            docker.withServer('tcp://vps196233.vps.ovh.ca:2375', '') {
                image = docker.build("simple-app", '-f Dockerfile .')
                node {
                container = image.run('-P')
                ip = container.port(8080)
                }
                try {
                    input message: "Is http://${ip} ok?", ok: 'Publish'
                } finally {
                    node { container.stop() }
                }
            }
        }
    }
    

## Use Docker server outside of Jenkins server
On Centos7

1. Create a file at `/etc/systemd/system/docker.service.d/startup_options.conf` with the below contents:
    ```
    # /etc/systemd/system/docker.service.d/override.conf
    [Service]
    ExecStart=
    ExecStart=/usr/bin/dockerd -H fd:// -H tcp://0.0.0.0:2376
    ```

    Note: The -H flag binds dockerd to a listening socket, either a Unix socket or a TCP port. You can specify multiple -H flags to bind to multiple sockets/ports. The default -H fd:// uses systemd's socket activation feature to refer to /lib/systemd/system/docker.socket.


2. Reload the unit files:

    ```$ sudo systemctl daemon-reload```

3. Restart the docker daemon with new startup options:

    ```$ sudo systemctl restart docker.service```

    Ensure that anyone that has access to the TCP listening socket is a trusted user since access to the docker daemon is root-equivalent.

    or run :

    ```sudo dockerd -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock &```

4. Then follow [this link](https://wiki.jenkins.io/display/JENKINS/Docker+Plugin) to install new cloud on Jenkins server :

    1. Admin jenkins : install Docker plugin
    2. Admin jenkins : configure system > cloud > Docker
        - Cloud configuration
        ![img](jenkins_cloud_config.png)
    3. New Freestyle project :
        - Git
        ![img](jenkinsjob_git.png "Git fetch")
        - Maven package compilation (.jar generation)
        ![img](jenkinsjob_mavenbuild.png "maven build")
        - Docker build and push on dockerhub with buildnumber as tag
        ![img](jenkinsjob_builddocker.png "Docker build ")
        - Docker run
        ![img](jenkinsjob_rundocker.png "Docker run")

After jobs success, Docker plugin shows Docker images and containers:
    ![img](jenkins_dockerplugin.png)

