# Docker

## start point : https://docker.goffinet.org/

## sources
Docker documentation
Docker formator : Petazzoni (see Youtube)

## tools
- gns3 : https://www.gns3.com/
- curl ipinfo.io/ip

## Usage Examples : 
- wordpress: https://docs.docker.com/compose/wordpress/#define-the-project
    + web : apache / nginx
    + php : differentes version
    + mySql
- drupal

## SWARM + KVM
Install KVM driver for docker-machine : https://github.com/dhiltgen/docker-machine-kvm
```Shell
sudo apt-get install libvirt-bin qemu-kvm
wget https://github.com/dhiltgen/docker-machine-kvm/releases/download/v0.10.0/docker-machine-driver-kvm-ubuntu16.04
sudo mv docker-machine-driver-kvm-ubuntu16.04 /usr/local/bin/docker-machine-driver-kvm
chmod +x /usr/local/bin/docker-machine-driver-kvm
```
Or create vm with virsh and acces them via SSH :

- list and start kvm nodes :
```
sudo virsh start node1
sudo virsh start node2
sudo virsh list
```
- console : escape is CTR+ALT_GR+]
```
sudo virsh console node1 
```