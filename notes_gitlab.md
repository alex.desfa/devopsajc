# Git

```git init``` ou ```git clone```

```git add``` ou ```git stage```

```git commit -a -m "Commit message" ```

```git diff --cached <file>```
    modifications part of next commit

```git rm --cached <file>```
    suppress file from index but no locally

```git remote set-url origin git@vps604887.ovh.net:Alex/projtest.git```
    change URL between http/ssh

```git remote add toto https://github.com/toto/monproject```
    add a remote repo to push/pull from

```git fetch [nom_du_repo_distant]```
    s'adresse au dépot distant et récupère toutes les données qu'on ne possède pas.
    dépot distant automatiquement ajouté sous le nom "origin"

```git push [depot_distant] [nom_de_la_branche]```
    git push origin master

```git remote show origin```

```git remote rename old_name new_name```
```git remote rm name```

```git tag -a v1.1.10 -m 'ma version 1.1.0 ```
    créer une etiquette annotée

```git push origin --tags```
    pousser les etiquettes locales sur le serveur

```git checkout --b v1.1.10```
```git checkout branch_name```
    change branch

```git branch```
    list branch and see where we are
```git branch -d <branche_to_be_deleted>```
    suppress a branch
    
```git log --oneline --decorate --graph --all```
    log in console

```git merge master hotfix```
    merge dev made on branch named hotfix to master




# Install Gitlab CE on Http
```
export SERVER_NAME="vps604887.ovh.net"
sudo yum install curl policycoreutils-python openssh-server
sudo yum install postfix
sudo systemctl start postfix
sudo systemctl enable postfix
sudo systemctl status 
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | sudo bash
EXTERNAL_URL="http://$SERVER_NAME" sudo yum install -y gitlab-ce
sudo vi  /etc/gitlab/gitlab.rb # check configuration especially external_url
sudo gitlab-ctl reconfigure
```

# Firewall
```
sudo yum -y install firewalld
sudo systemctl start firewalld
sudo systemctl enable firewalld
sudo firewall-cmd --permanent --add-service ssh
sudo firewall-cmd --permanent --add-service http
sudo firewall-cmd --permanent --add-service https
sudo firewall-cmd --permanent --add-port 6000/tcp
sudo firewall-cmd --reload
sudo firewall-cmd --list-all
```
# Gitlab tutos
- Gitlab : https://docs.gitlab.com/ee/user/project/import/
- Git & Gitlab : https://git-scm.com/book/fr/v2/Git-sur-le-serveur-GitLab
- CI/CD : https://blog.eleven-labs.com/fr/introduction-gitlab-ci/
- Book : https://page.gitlab.com/rs/194-VVC-221/images/Starting%20and%20Scaling%20DevOps%20in%20the%20Enterprise.pdf

# Gitlab installation on http://vps604887.ovh.net/
https://docs.gitlab.com/ee/administration/restart_gitlab.html#omnibus-gitlab-reconfigure
https://www.howtoforge.com/tutorial/how-to-install-and-configure-gitlab-ce-on-centos-7/#step-install-packages

# swap
https://www.digitalocean.com/community/tutorials/how-to-add-swap-on-centos-7

# reduce memory
https://edspencer.me.uk/2017/07/30/reducing-the-amount-of-memory-used-by-gitlab/
