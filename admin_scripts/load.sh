#!/bin/bash
source $HOME/.bashrc

load=`cat /proc/loadavg | awk '{print "m="$1,"5m="$2,"15m="$3}' 2>&1`
#read load1 load5 load15 r < /proc/loadavg 
echo -e "$(date +%D-%T) :\tloads values\t${RED}$load${BLACK}" >> $HOME/cron.log
