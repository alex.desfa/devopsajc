#!/bin/bash
source $HOME/.bashrc
echo -e "$(date +%D-%T) :\t$USER executed a ${GREEN}CRON JOB${BLACK}\t$SHELL PWD=$PWD" 2>$HOME/error.log >> $HOME/cron.log
