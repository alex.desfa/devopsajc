#!/bin/bash
function usage 
{
    echo -e "$0 reset\t reset variable to 0"
    echo -e "$0 [+/-]\t increment or decrement var"
}

function printerr
{
    echo "Error : $1. Exiting"
    exit 1
}

varFile="/tmp/incr.txt"

case $1 in
    reset)
        echo 0 > $varFile
        ;;
    random)
        echo $RANDOM > $varFile
        ;;
    get)
        echo "value of variable is $(cat $varFile)"
        ;;
    +)
        test -f $varFile || printerr "File $varFile not found"
        var=$(cat $varFile)
        (( var = var + 1 ))
        echo $var > $varFile
        ;;
    -)
        test -f $varFile || printerr "File $varFile not found"
        var=$(cat $varFile)
        test -f $varFile || exit 1
        (( var = var - 1 ))
        echo $var > $varFile
        ;;
    *)
        usage
        exit 1
esac



