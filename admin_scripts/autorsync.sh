#!/bin/bash
source $HOME/.bashrc

function usage
{
  echo -e "$0\t -h|--help \t Show this help"
  echo -e "$0\t -i|--interactive \tInteractive mode\n"
  echo -e "$0\t [MODE:-l/-r] [SRC_DIR] [user@server:REMOTE_DIR] REMOTE_PORT\n"

  echo -e "Arguments:"
  echo -e "\t MODE\t\t -l: for local, -r: for remote"
  echo -e "\t SRC_DIR\t is the local directory to sync"
  echo -e "\t user@server:REMOTE_DIR\t is the SSH path to use for sync"
  echo -e "\t REMOTE_PORT\t is the SSH port to use"
}

function perror_exit
{
  echo -e "${RED}Error: $*${BLACK}"
  exit 1
}

function print
{
  echo -e "${GREEN}Info:${BLACK} $*"
}


function enter_args
{
  echo "Enter mode [local/remote]:"
  select entry in "local" "remote"
  do
    case $entry in 
      "local") MODE="local";;
      "remote")MODE="remote";;
      *) 
        perror_exit "Bad mode value";;
    esac
    break
  done
  
  read -p "Enter local directory path:" SRC_DIR
  if [ ! -e $SRC_DIR ]; then
    perror_exit "Error : SRC_DIR not found ($SRC_DIR)"
  fi

  read -p "Enter $MODE sync path:" REMOTE_DIR
  if [ -z "$REMOTE_DIR" ]; then
    perror_exit "Error : bad REMOTE_DIR ($REMOTE_DIR)"
  fi

  if [ $MODE = "remote" ];then
    read -p "Enter remote user:" SSH_USER
    if [ -z $SSH_USER ];then
      perror_exit "Error : bad SSH_USER ($SSH_USER)"
    fi
    read -p "Enter server name:" SSH_SERVER
    if [ -z $SSH_SERVER ];then
      perror_exit "Error : bad SSH_SERVER ($SSH_SERVER)"
    fi
    SSH_URL="$SSH_USER@$SSH_SERVER"

    read -p "Enter remote port:" SSH_PORT
    if [ -z $SSH_PORT ];then
      perror_exit "Error : bad SSH_PORT ($SSH_PORT)"
    fi
  fi

  echo "Compress backup target directory [yes/no]:"
  select comp in "yes" "no"
  do
    case $comp in 
      "yes"|"no") COMPRESS_OPT=$comp;;
      *) 
        perror_exit "Bad compression value";;
    esac
    break
  done
}

function check_args
{
  # check arguments
  if [ $# -eq 0 ];then
    usage
    exit 0
  elif [ $# -lt 3 ]; then
    case $1 in
      -h|--help) usage;exit 0;;
      -i|--interactive) 
        print "Entering interactive mode."
        enter_args
        ;;
      *)
        perror_exit "Error : bad arguments"
        ;;
    esac
  else
    case $1 in
      -h|--help) usage;exit 1;;
      -l) MODE="local";;
      -r) MODE="remote";;
      *) 
        perror_exit "Error: bad value for MODE"
      ;;
    esac
  
    #check SRC_DIR
    if [ ! -e $2 ]; then
      perror_exit "Error : SRC_DIR not found ($2)"
    fi
  
    #check REMOTE_PATH
    if [ -z $3 ]; then
      perror_exit "Error : bad REMOTE_PATH ($3)"
    else
      if [ $MODE = "remote" ]; then
        re='[[:alnum:]|-]*@[[:alnum:]|-]*:[[:alnum:]|[:space:]|\/]*'
        if [[ $3 =~ $re ]]; then
          print "Good SSH argument ($3)"
        else
          perror_exit "Bad SSH argument ($3)"
        fi
      fi
    fi
  
    # check REMOTE_PORT
    if [ ! -z $4 ];then
      SSH_PORT="$4"
      if [ $MODE = "remote" ]; then
        re='[0-9]+$'
        if [[ $SSH_PORT =~ $re ]]; then
          print "Good SSH_PORT argument"
        else
          perror_exit "Bad SSH_PORT argument"
        fi
      fi
    else
      SSH_PORT="22"
    fi
  
    #verify SSH_URL and REMOTE_DIR
    SSH_URL=$(echo "$3" | cut -d : -f 1)  
    REMOTE_DIR=$(echo "$3" | cut -d : -f 2)
    SRC_DIR=$2    
    COMPRESS_OPT="yes"
  fi
}

function local_compress_backup
{
    # $1 is the RSync dir path
    print "Compressing localy $1/backup to $1/backup.tgz"
    tar czf $1/backup.tgz $1/backup
    rm -rf $1/backup/
}

function remote_compress_backup
{
  # $1 is the RSync dir path
  print "Compressing remotely $1/backup to $1/backup.tgz"

  ssh -p $SSH_PORT $SSH_URL test -e $BACKUP_DIR
  if [ $? -eq 0 ]; then
    print "Info : Backup directory found." 
    ssh -p $SSH_PORT $SSH_URL tar czf $1/backup.tgz $1/backup
    ssh -p $SSH_PORT $SSH_URL rm -rf $1/backup/
  else
    print "Info : no backup directory found"
  fi    
}

###
# main operations
###

check_args $*

BACKUP_DIR=$REMOTE_DIR/backup
RSYNC_LOCAL_OPT="-aq --delete --backup --backup-dir=$BACKUP_DIR"
RSYNC_REMOTE_OPT="-avz --delete --backup --backup-dir=$BACKUP_DIR"

if [ $MODE = "local" ];then
  ## LOCAL
  print "Local mode, will use :\n"\
        "\t src : $SRC_DIR\n"\
        "\t dest : $REMOTE_DIR\n"\
        "\n"

  if [ ! -d "$BACKUP_DIR" ];then
    print "Info : creating $BACKUP_DIR"
    mkdir -p $BACKUP_DIR
  fi

  #local
  rsync $RSYNC_LOCAL_OPT $SRC_DIR $REMOTE_DIR
  if [ $? -eq 0 ];then
    print "Info : rsync success"
  else
    perror_exit "rsync failure"
  fi

  #compress backup
  if [ $COMPRESS_OPT = "yes" ];then 
    local_compress_backup $REMOTE_DIR
  else
    print "Compresion option is set to: $COMPRESS_OPT. Skipping"  
  fi  

else
  ## REMOTE

  print "Remote mode, will use :\n"\
        "\t src : $SRC_DIR\n"\
        "\t dest : $SSH_URL:$REMOTE_DIR\n"\
        "\t port : $SSH_PORT\n"\
        "\t options :\n\t$RSYNC_REMOTE_OPT\n"\
        "\n"

  #remote rsync
  rsync -e "ssh -p $SSH_PORT" $RSYNC_REMOTE_OPT $SRC_DIR $SSH_URL:$REMOTE_DIR
  if [ $? -eq 0 ];then
    print "Info : rsync success"
  else
    perror_exit "rsync failure"
  fi

  # zip and clean backup  
  if [ $COMPRESS_OPT = "yes" ];then 
    remote_compress_backup $REMOTE_DIR
  else
    echo "Compresion option is set to: $COMPRESS_OPT. Skipping"  
  fi
fi
 


