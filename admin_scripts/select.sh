#!/bin/bash

function f1()
{
  echo "Action 1"
}

function f2()
{
  echo "Action 2"
}

PS3="Quel est votre choix:"
select result in '1' '2'
do
  case $result in
    1)f1;;
    2)f2;;
    *)echo "Mauvais choix";;
  esac
  echo "VOUS AVEZ SELECTIONNE $result"
  break
done
