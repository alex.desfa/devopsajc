## Install CentOS7

## Gitbook
https://alternative-rvb.gitbook.io/linux/

## Notes
  - ```yum install redhat-lsb-core.x86_64 ```
  - ```visudo```
  - ```screenfetch```
  - ```most```
  - ```usermod -aG vboxsf alex```
  ATTENTION : ```usermod -aG != usermod -G != usermod -g```
  - ```newgrp vboxsf (to change primary group)```
  - https://github.com/junegunn/fzf - fuuzy search in shell
  - lsbsk - list the tree of all disks
  - shellcheck - check errors in shell scripts
  - translate-shell - translate strings in bash
  - ```rsync -arv -e 'ssh -p 22223' myDir/ alexandre@ovh-serveur:~/Documents/syncedDir```
