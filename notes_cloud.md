# Cloud

## IAAS : Infrastructure As A Service
* resources materielles, serveurs virtualisés à la demande
* connectivité internet garantie
* config sys
* application web
* lancement du service
* itérations sur les perf

Les acteurs :
- AWS : S3, EC2
- OpSource, MS Azur
- Gandi, OVH

## PAAS : Platform As A Service
* PAAS = IAAS + ... logiciel / Base de Donnée / Intégration SOA / Runtimes
* plateforme logicielle pour un logiciel donné , exemple : LAMP

Les acteurs :
- Heroku / AWS / ...
- appli mobiles & sociales
- banques / industries

## SAAS : Software As A Service
* application mise à dispo sans soucis de materiel / puissance
* appli & services web
* tout est chez le fournisseur de SAAS
* craintes de securité : localisation des données, juridique

Les acteurs :
- Gmail / Office365
- Visio conf
- ...

## Topologie
- Cloud Privé
- Cloud Public
- Cloud Hybrid
- Cloud Virtual

## Cas pratique
FoodTruck : site marchand + applis mobile

Structure en containers :
- Web1 : Web Frontend (Wordpress ou PHP/Bootstrap ou Angular)
- Web2 : Mobile Frontend
- Web3 : ...
- WebServices API (JSON / XML / ...) pour que les WebX communiquent avec la DB
- DB : MySQL( load balancing + mirroring )

```
LOAD BALANCING
|    |     |
W1   W2    W3
|    |     |
  MIRRORING
     |
 DB SERVERS
```

## CDN : content delivery network
Ce réseau est constitué :
- de serveurs d'origine, d'où les contenus sont répliqués ;
- de serveurs périphériques, typiquement déployés à plusieurs endroits géographiquement distincts, où les contenus des serveurs d'origine sont répliqués ;
- d'un mécanisme de routage permettant à une requête utilisateur sur un contenu d'être servie par le serveur le « plus proche », dans le but d’optimiser le mécanisme de transmission / livraison.

# AWS

aws configure
AWS Access Key ID [None]: AKIAIUIISVSKKJ3DJFQQ 
AWS Secret Access Key [None]: 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

Install wordpress on Ubuntu : https://doc.ubuntu-fr.org/wordpress

MySql info:

    mysql -u user -p (local)
    mysql -h servername -u user -P port -p (remote)

    show databases
    use db1
    show tables


MySql credentials:

    ajcMysqlTraining
    alex / alex!123+456

## EC2 
service de IAAS/PAAS
## LIGHTSAIL
service de PAAS/SAAS

## S3
stockage de type drive
## AWS CLI
console amazon en ligne de commande
## RDS
mySql
## DynamoDB
noSql
## Lambda
FAAS : function as a service
NodeJS : 
- framework javascript qui fournit des outils front et backend
- env de dev

```
sudo npm install -g serverless
serverless create -t aws-nodejs
```

Voir : https://medium.com/the-node-js-collection/building-your-first-serverless-app-in-node-js-with-aws-lambda-s3-api-gateway-4d87e808d9cc

## ECS/ECK
service de type docker
aws ecr get-login --no-include-email --region us-east-1
docker build -t ecr_ajc-devops .
docker tag ecr_ajc-devops:latest 810595826969.dkr.ecr.us-east-1.amazonaws.com/ecr_ajc-devops:latest
docker push 810595826969.dkr.ecr.us-east-1.amazonaws.com/ecr_ajc-devops:latest

# Azure
Cloud Privé : Microsoft Azure Stack
Cloud Public : Microsoft Azure
Cloud Hybride = Privé + Public

## Active Directory :
- profils utilisateurs
- gestion des configuration

## DMZ
Zone Démilitarisée
> C'est un sous-réseau séparé du réseau local et isolé de celui-ci et d'Internet (ou d'un autre réseau) par un pare-feu. Ce sous-réseau contient les machines étant susceptibles d'être accédées depuis Internet, et qui n'ont pas besoin d'accéder au réseau local. 


## Azure VM Creation via CLI& Sandbox
https://docs.microsoft.com/fr-fr/learn/paths/azure-fundamentals/

- Install

    https://docs.microsoft.com/fr-fr/cli/azure/install-azure-cli-apt?view=azure-cli-latest

- Login
    ```
    az login
    ```
- Creation
    ```
    az vm create \
    --name myVM \
    --resource-group eb07960f-a9a8-41b4-a8ff-433aae8b0f1d \
    --image Win2016Datacenter \
    --size Standard_DS2_v2 \
    --location eastus \
    --admin-username azureuser
    ```
- Add extension for IIS Web Server
    ```
    az vm extension set \
    --resource-group [sandbox resource group name] \
    --vm-name myVM \
    --name CustomScriptExtension \
    --publisher Microsoft.Compute \
    --settings '{"fileUris":["https://raw.githubusercontent.com/MicrosoftDocs/mslearn-welcome-to-azure/master/configure-iis.ps1"]}' \
    --protected-settings '{"commandToExecute": "powershell -ExecutionPolicy Unrestricted -File configure-iis.ps1"}'
    ```
- Port configuration
    ```
    az vm open-port \
    --name myVM \
    --resource-group [sandbox resource group name] \
    --port 80
    ```

- Verification
    ```
    az vm show \
    --name myVM \
    --resource-group [sandbox resource group name] \
    --show-details \
    --query [publicIps] \
    --output tsv
    ```
- Rescale
    ```  
    az vm resize \
    --resource-group [sandbox resource group name] \
    --name myVM \
    --size Standard_DS3_v2
    ```
- Verification
    ```
    az vm show \
    --resource-group a9334846-e172-49e8-a392-0cab14ec157f \
    --name myVM \
    --query "hardwareProfile" \
    --output tsv
    ```
# Wordpress on IIS

Install IIS
Install Web Platform Installer
Add Wordpress

Modify MySQL DB :

    UPDATE wp_options SET option_value = replace(option_value, 'http://localhost/wordpress', 'http://40.85.180.38/wordpress/') WHERE option_name = 'home' OR option_name = 'siteurl';
    UPDATE wp_posts SET guid = replace(guid, 'http://localhost/wordpress','http://40.85.180.38/wordpress/');
    UPDATE wp_posts SET post_content = replace(post_content, 'http://localhost/wordpress', 'http://40.85.180.38/wordpress/');
    UPDATE wp_postmeta SET meta_value = replace(meta_value,'http://localhost/wordpress','http://40.85.180.38/wordpress/');