## Méthode RPBD: pour mener un premier entretien client
R : Réalité (situation actuelle)
P : Projection (client -> situation idéale = But)
B : Besoin
D : Demande (attente du client : qualité requise, technique) -> à creuser

## Savoir / Savoir-faire / Savoir-être
Travailler son côté gauche peu aider à developer sa creativité
assertivité : capacité à s'affirmer
60000 pensées/jour

## 3 modes de communication:
- verbal : mots - 7%
- para-verbal : silence, intonation, débit, voix, rythme - 38%
- non verbal : gestes - 55%



## Les croyances / en AT selon Taibi Kahler :
- sois parfait : perfectionniste,
- sois fort : mal à déléguer, peu tout faire toute seule, autonome, surchargé de travail
- dépêche-toi : lent, dans le général le superflu, impatient
- fais plaisir : neglige ses propres besoins, serviable et consensuel, a du mal à dire non
- fais un effort : choisi le chemin le plus difficile pour trouver la satisfaction


## Quotient émotionnel:
- sérénité : 13/20 (vs anxiété)
- optimisme : 17/20 (vs défaitisme)
- sens des réalités : 15/20 (vs idéalisation)
- maîtrise de soi : 16/20 (vs impulsivité, émotivité)
- empathie : 15/20 (vs indifférence)
      = 76/100

## modèle Goleman 
