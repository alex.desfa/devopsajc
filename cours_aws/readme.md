# How to use AWS ECR/ECK

## Signup, add IAM user
https://docs.aws.amazon.com/AmazonECR/latest/userguide/get-set-up-for-amazon-ecr.html

## Install and configure AWS Cli

```bash
$ aws configure
AWS Access Key ID [****************JFQQ]: AKIAJ63ICLUZFREK63FQ
AWS Secret Access Key [****************iSLf]: 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
Default region name [useast-1]: eu-west-1
Default output format [None]:
```
    
## Install Docker and create image
With dockerfile or pull it from somewhere:

```bash
docker login
docker pull desfa/maven:11
docker run -d -p 8080:8080 desfa/maven:11
```

## Tag the image and push it to Amazon ECR

```bash
# create ECR repo
aws ecr create-repository --repository-name maven-repository
{
"repository": {
    "repositoryArn": "arn:aws:ecr:eu-west-1:817926166193:repository/maven-repository",
    "registryId": "817926166193",
    "repositoryName": "maven-repository",
    "repositoryUri": "817926166193.dkr.ecr.eu-west-1.amazonaws.com/maven-repository",
    "createdAt": 1547588220.0
 }
}

# tag local docker image
docker tag desfa/maven:11 817926166193.dkr.ecr.eu-west-1.amazonaws.com/maven-repository:latest

# login to AWS ECR
aws ecr get-login --region eu-west-1 --no-include-email --registry-ids 817926166193 | sh

# will run
docker login -u AWS -p eyJwYXlsb2FkIjoiYmxKYkZNc0NrbDJkVzVzTjV0N3k1Y2x1WEFNZzdRUUp5WEdvNUFYRkFDN2o1OWRLRzBpSFplNkVWME5uSHJ3MDIvTWJjK3VYRFVDUElCZSt4WW5CNEc1a3JaMzdBNmk0c21tcDFHSmMxdkRUR3c2UVRsWFZLRzJ3R2hxc3d3YXJDNmIxRXBCbUJBcXI3ZVZpUGlzMy9Cc0RlTE5MNUtNTjA3K1YvbHVLbW1CbmFQSXE5WURZc0dSSTlXU1d6b09GUHhIUXV0VmtPd01RT1cvSXA1VTg0OGlFSkZMS242dEFWcGZ3a05idDVFUENjT1UyWk1hVnNpMnlIZ3ZXUVkxaC9hMUhGa2k0RUpkZG9hKzNucXYxSy9iNTRjeUVCeWVWYWE0RkZpNlFUSUNFb3E0Q1pTTSt5OXlIODRxK1k0N0VnS2haR3c2RGhvV0lTWjJwR3BhMTQ2M0JPcDBKNHRSRkVzRzVNYm9mcThScm1iQW50dktSRVlrZDdXYWNVeE9FbEJzV1IrU3A4TWVIVXIvWVRTOG9DWE02VVA0c2FHcXJyTWNpSHNTSDE0bGdrSEJ0Zk5paHYzS2tCMW54c2N5UWRxN0hmZ3lLSW13ZVAxN2UvT0xNdC9veDBZR0xFRkRaTzRrbzY4ZDBvaGQ4T0VUOHpkY0ZMQldjZXkxUUlvbmkveUdrNS93TXQ1ZFdLMU5MMGJNWHJydC9IU3VFNnBXL0ZGNXVTNVF6RS9CdWdMdDZkV0xFdVJBQ3VFNVVjM25yS1Q0RFdQdWxXbGhvT2gwc2M4YnVtajJIaFBDNExockYyZHVPWkI5S2lpY2RybEFZL0VyMTdLT1czSTFDYWVhUTZoYVZnS1BrZkhrbjF2UmxIYkdrY09yMzV5a3htd3dubGh1N21XNW4vbUVtcU1kZnhNbGlMdDFTTHRNYm9vQTRoMUNmWXZnRktybGlCS2tuSk0vK2E1R1lTclpBRUhaVW5vNG5uWGgyeWIvOTEybDFNTDRKTGtrQkdYc2RLdjYxeDBuODJZL2kzdHJZWHA2WnZ3M05ydXlJS2FteUN4QTV6NU0yOTFMamNNeXNWQ241bXM3aURhOUhUekVsQVJXcGN3bjNkWWV1bE1oQi8rUm11dVlScWJBeGRwZFp5dzN4Y2ZVVStGZVN0UHpELzRCZVF0bmh0Z1FSaFJkQSsrUXFZZEM0MURyQytiZlp6ZFhmcVc1MTdmcmR0bGlORTlhMGRvTzI2TFAxTm8zaVM2VXRubmVwaWE3R2RFNnB4UlhWTlpXTndMc0RvdTIrcElVWFdRUWtJSy8zTE1qeStNYllFZFFHKy92UzZiU3l0VXJDK2h4TzBkM1pTRmlFTkd4c2J4cll2YVEzcUJIQVdCQkJRY2RZQko0NXhoYlU5aU5hSHhoYUF6OHB0RlVsbkRFUmk5Zk1rc2JzbEE4SVJGdXdSQmNad0VTS2RyVXIrQWtJM3pXVm9lSFNkdzJSblVITE5ZRWYyOEh4VUhFc29nPT0iLCJkYXRha2V5IjoiQVFFQkFIaCtkUytCbE51ME54blh3b3diSUxzMTE1eWpkK0xOQVpoQkxac3VuT3hrM0FBQUFINHdmQVlKS29aSWh2Y05BUWNHb0c4d2JRSUJBREJvQmdrcWhraUc5dzBCQndFd0hnWUpZSVpJQVdVREJBRXVNQkVFREpQUlhZclRyRHNQWVBnM2FBSUJFSUE3TDVjc3BKbFM1MUZ6QnA5K1p4YmQxZ1FZS0tLcWxvbWdOWEpUdTNrYVRPUElxYkFxaU90Ykx5ZTZuRXRBZS9CSTVyS1JiV2E2dVc2SzY1Yz0iLCJ2ZXJzaW9uIjoiMiIsInR5cGUiOiJEQVRBX0tFWSIsImV4cGlyYXRpb24iOjE1NDc2MzE3NTl9 https://817926166193.dkr.ecr.eu-west-1.amazonaws.com
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
WARNING! Your password will be stored unencrypted in /home/desfa/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded

# push to AWS repo
docker push 817926166193.dkr.ecr.eu-west-1.amazonaws.com/maven-repository:latest"

# deploy the service

# run it
warning: 
    Check this : https://stackoverflow.com/questions/36523282/aws-ecs-error-when-running-task-no-container-instances-were-found-in-your-clust
    Created using jenkins.pem

```

## Delete ECR repo

```bash
# not to be charged
aws ecr delete-repository --repository-name hello-repository --force
```

## Deploy
1. From Console : https://aws.amazon.com/getting-started/tutorials/deploy-docker-containers/

    See Screenshots :
    ![Task Definition](aws_task_def.png)
    ![Launch Status](launch_status.png)


2. From CLI :


```bash
# Create security group for the cluster
aws ec2 create-security-group --group-name maven-ecs-sg --description mavn-ecs-sg
{
    "GroupId": "sg-04d3609ab8d5819e6"
}

# Create cluster
aws ecs create-cluster --cluster-name maven-cluster --region eu-west-1
{
  "cluster": {
    "clusterArn": "arn:aws:ecs:eu-west-1:817926166193:cluster/maven-cluster",
    "clusterName": "maven-cluster",
    "status": "ACTIVE",
    "registeredContainerInstancesCount": 0,
    "runningTasksCount": 0,
    "pendingTasksCount": 0,
    "activeServicesCount": 0,
    "statistics": []
  }
}

# Create Instance for ECS instantiation
aws ec2 run-instances --image-id ami-05b65c0f6a75c1c64 --count 1 --instance-type t2.micro --key-name jenkins --user-data file://user-data.txt --security-group-ids sg-04d3609ab8d5819e6 --iam-instance-profile Arn=$ecsInstanceRole_ARN --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=ECS-Instance-MAVEN-CLUSTER}]'
{
    "Groups": [],
    "Instances": [
        {
            "AmiLaunchIndex": 0,
            "ImageId": "ami-05b65c0f6a75c1c64",
            "InstanceId": "i-0dc261bf0b4093b44",
            "InstanceType": "t2.micro",
            "KeyName": "jenkins",
            "LaunchTime": "2019-01-16T21:12:42.000Z",
            "Monitoring": {
                "State": "disabled"
            },
            "Placement": {
                "AvailabilityZone": "eu-west-1c",
                "GroupName": "",
                "Tenancy": "default"
            },
            "PrivateDnsName": "ip-172-31-29-56.eu-west-1.compute.internal",
            "PrivateIpAddress": "172.31.29.56",
            "ProductCodes": [],
            "PublicDnsName": "",
            "State": {
                "Code": 0,
                "Name": "pending"
            },
            "StateTransitionReason": "",
            "SubnetId": "subnet-c601748e",
            "VpcId": "vpc-e76b4381",
            "Architecture": "x86_64",
            "BlockDeviceMappings": [],
            "ClientToken": "",
            "EbsOptimized": false,
            "Hypervisor": "xen",
            "NetworkInterfaces": [
                {
                    "Attachment": {
                        "AttachTime": "2019-01-16T21:12:42.000Z",
                        "AttachmentId": "eni-attach-0ef86c6a30deddf32",
                        "DeleteOnTermination": true,
                        "DeviceIndex": 0,
                        "Status": "attaching"
                    },
                    "Description": "",
                    "Groups": [
                        {
                            "GroupName": "maven-ecs-sg",
                            "GroupId": "sg-04d3609ab8d5819e6"
                        }
                    ],
                    "Ipv6Addresses": [],
                    "MacAddress": "06:87:bf:98:d6:48",
                    "NetworkInterfaceId": "eni-00ba24a834dda2570",
                    "OwnerId": "817926166193",
                    "PrivateDnsName": "ip-172-31-29-56.eu-west-1.compute.internal",
                    "PrivateIpAddress": "172.31.29.56",
                    "PrivateIpAddresses": [
                        {
                            "Primary": true,
                            "PrivateDnsName": "ip-172-31-29-56.eu-west-1.compute.internal",
                            "PrivateIpAddress": "172.31.29.56"
                        }
                    ],
                    "SourceDestCheck": true,
                    "Status": "in-use",
                    "SubnetId": "subnet-c601748e",
                    "VpcId": "vpc-e76b4381"
                }
            ],
            "RootDeviceName": "/dev/xvda",
            "RootDeviceType": "ebs",
            "SecurityGroups": [
                {
                    "GroupName": "maven-ecs-sg",
                    "GroupId": "sg-04d3609ab8d5819e6"
                }
            ],
            "SourceDestCheck": true,
            "StateReason": {
                "Code": "pending",
                "Message": "pending"
            },
            "Tags": [
                {
                    "Key": "Name",
                    "Value": "ECS-Instance-MAVEN-CLUSTER"
                }
            ],
            "VirtualizationType": "hvm",
            "CpuOptions": {
                "CoreCount": 1,
                "ThreadsPerCore": 1
            }
        }
    ],
    "OwnerId": "817926166193",
    "ReservationId": "r-08386b948abf3e714"
}
Check role is EcsInstanciation, check security group, check it is part of the cluster



# Task definition
aws ecs register-task-definition --family ${FAMILY} --cli-input-json file://${WORKSPACE}/${NAME}-v_${BUILD_NUMBER}.json --region ${REGION}
{
    "taskDefinition": {
        "taskDefinitionArn": "arn:aws:ecs:eu-west-1:817926166193:task-definition/maven-test:1",
        "containerDefinitions": [
            {
                "name": "maven-test",
                "image": "817926166193.dkr.ecr.eu-west-1.amazonaws.com/maven-repository:v_1",
                "cpu": 1,
                "memory": 512,
                "portMappings": [
                    {
                        "containerPort": 8080,
                        "hostPort": 8080,
                        "protocol": "tcp"
                    }
                ],
                "essential": true,
                "environment": [],
                "mountPoints": [],
                "volumesFrom": []
            }
        ],
        "family": "maven-test",
        "revision": 1,
        "volumes": [],
        "status": "ACTIVE",
        "requiresAttributes": [
            {
                "name": "com.amazonaws.ecs.capability.ecr-auth"
            }
        ],
        "placementConstraints": [],
        "compatibilities": [
            "EC2"
        ]
    }
}

# create the service
aws ecs create-service --service-name ${SERVICE_NAME} --desired-count 1 --task-definition ${FAMILY} --cluster ${CLUSTER} --region ${REGION}
{
    "service": {
        "serviceArn": "arn:aws:ecs:eu-west-1:817926166193:service/maven-test-service",
        "serviceName": "maven-test-service",
        "clusterArn": "arn:aws:ecs:eu-west-1:817926166193:cluster/maven-cluster",
        "loadBalancers": [],
        "serviceRegistries": [],
        "status": "ACTIVE",
        "desiredCount": 1,
        "runningCount": 0,
        "pendingCount": 0,
        "launchType": "EC2",
        "taskDefinition": "arn:aws:ecs:eu-west-1:817926166193:task-definition/maven-test:1",
        "deploymentConfiguration": {
            "maximumPercent": 200,
            "minimumHealthyPercent": 100
        },
        "deployments": [
            {
                "id": "ecs-svc/9223370489182430416",
                "status": "PRIMARY",
                "taskDefinition": "arn:aws:ecs:eu-west-1:817926166193:task-definition/maven-test:1",
                "desiredCount": 1,
                "pendingCount": 0,
                "runningCount": 0,
                "createdAt": 1547672345.371,
                "updatedAt": 1547672345.371,
                "launchType": "EC2"
            }
        ],
        "events": [],
        "createdAt": 1547672345.371,
        "placementConstraints": [],
        "placementStrategy": [],
        "schedulingStrategy": "REPLICA"
    }
}


# run task
aws ecs run-task --cluster ${CLUSTER} --task-definition ${NAME} --launch-type EC2 --count 1
{
    "tasks": [
        {
            "taskArn": "arn:aws:ecs:eu-west-1:817926166193:task/d1d818e3-fe6f-478d-bd44-5498ff6b7b1c",
            "clusterArn": "arn:aws:ecs:eu-west-1:817926166193:cluster/maven-cluster",
            "taskDefinitionArn": "arn:aws:ecs:eu-west-1:817926166193:task-definition/maven-test:1",
            "containerInstanceArn": "arn:aws:ecs:eu-west-1:817926166193:container-instance/6c3f3272-ff7a-4471-9f5b-3e3defbe607b",
            "overrides": {
                "containerOverrides": [
                    {
                        "name": "maven-test"
                    }
                ]
            },
            "lastStatus": "PENDING",
            "desiredStatus": "RUNNING",
            "cpu": "1",
            "memory": "512",
            "containers": [
                {
                    "containerArn": "arn:aws:ecs:eu-west-1:817926166193:container/c15b3a05-dbc5-4f47-88c8-3dc20af49abe",
                    "taskArn": "arn:aws:ecs:eu-west-1:817926166193:task/d1d818e3-fe6f-478d-bd44-5498ff6b7b1c",
                    "name": "maven-test",
                    "lastStatus": "PENDING",
                    "networkInterfaces": []
                }
            ],
            "version": 1,
            "createdAt": 1547673494.74,
            "group": "family:maven-test",
            "launchType": "EC2",
            "attachments": []
        }
    ],
    "failures": []
}

```

See : https://foxutech.com/how-to-create-aws-ecs-using-aws-cli/

Script it with : https://github.com/AlexDesfa/ecs-deployment/blob/master/bin/deploy

