## TD3:

### Ex. 2 :
2. le contenu d'un lien symbolique est le même que la cible du lien
3. un lien symbolique a toujours les droits 777
4. si on supprime la cible d'un lien on ne supprime pas le lien, mais le contenu disparait
5/6. le contenu d'un lien physique est le même que la cible du lien
   un lien physique a les mêmes droits que la cible
   si on supprime le lien physique, le fichier source subsiste
7. pour faire un lien physique il faut avoir accès au fichier source

### Ex.3
```alias lla='ls -la --color=auto'```

## TD4:

### Ex.A
1. ```
   ls $HOME/test/rep1/rep4/fic6
   ls $HOME/test/*/*/fic6
   ls $HOME/test/rep[1-9]/rep[1-9]/fic6
   ```

### Ex.B
un lien physique a les mêmes droits que la cible
un lien symbolique a toujours les droits 777

## TD5:

Donner une commande qui renvoie la liste des noms des variables d'environnement sans les valeurs associées. La liste doit être triée en ordre alphabétique.  
```
printenv | cut -d "=" -f 1 | sort -d
```

Donner une commande qui renvoie le nombre de répertoires déclarés dans la variable PATH
```
echo $PATH | sed 's/:/\n/g' | wc -l
```
