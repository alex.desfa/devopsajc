# Lignes de commande

## Site web & contact
https://ligne-de-commande.goffinet.org/
goffinet@goffinet.eu

## Livres / Blogs:
Christophe Blaess - bouquins Linux : https://www.blaess.fr/christophe/livres/programmation-systeme-sous-linux/
Laurent Bloch : https://www.laurentbloch.net/MySpip3/
http://www.bortzmeyer.org/rfcs.html
Bouchaudy - Administration Linux

## Notes
  * scaleway - machines virtuelles sur le cloud
  * OpenWRT - OS embarqué
  * Xming - X server for windows
  * jq - parse JSON in shell
  * acl - liste d'utilisateur
  * loadkeys


## Exercices Vi
  - Quelle est la procédure pour effacer le contenu d'un fichier de 100 lignes
    et d'en ajouter un nouveau contenu ?
    ```
    >d100d
    ou
    :$ puis dgg
    ou dG
    ```
  - Quelles sont les procédures pour ajouter une ligne en dernière position d'un fichier avec le shell ? avec VI ?
    ```
    > Go to end of doc in vi: G
    > echo "something" >> file
    ```
  - Quelles sont les procédures pour remplacer dans un fichier tous les motifs de recherche avec VI ? avec le shell ?
    ```
    > Dans vim : :%s/old/new/g
    > en shell : sed 's/cursor/BODY/g' TEST
    ```

## Exercices Arbo
Challenge 4. : Sauvegarder des fichiers avec leurs attributs sur base d'une recherche.
  ```find ./ -name "fichiers_[1..2]*" -exec cp -avf {} ../backup_dir/ \;
    ```

## Droits
Challenge 5 : Créer des utilisateurs, un groupe commun sur un dossier local partagé.
