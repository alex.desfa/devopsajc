# Openshift

- clusters
- pod : unité la plus simple
- conteneurisation : basé sur Kubernetes
- Minishift Installation : 
  - https://docs.okd.io/latest/minishift/getting-started/preparing-to-install.html
  - https://www.novatec-gmbh.de/en/getting-started-minishift-openshift-origin-one-vm/

## Openshift objects diagram
```mermaid
graph TD;
  Client-->Cluster;
  Cluster-->Node;
  Cluster-->InfraNode; 
  Node-->POD1;
  Node-->POD2;
  Node-->PODN;
  InfraNode-->iPOD1;
  InfraNode-->iPOD2;
  InfraNode-->iPODN;
  POD1-->APP1;
  POD2-->APP2;
  PODN-->APPN;
  iPOD1-->iAPP1;
  iPOD2-->iAPP2;
  iPODN-->iAPPN;
  APP1-->STORAGE;
  APP2-->STORAGE;
  APPN-->STORAGE;
  iAPP1-->STORAGE;
  iAPP2-->STORAGE;
  iAPPN-->STORAGE;
  APP1-->DATABASE;
  APP2-->DATABASE;
  APPN-->DATABASE;
  iAPP1-->DATABASE;
  iAPP2-->DATABASE;
  iAPPN-->DATABASE;
  
```
## Devops Tools Diagram
![img](devops_diagram.png "Devops Tools Diagram")

## TP deployment : Docker > Gitlab > Openshift
0. Make sure DNS is accessible before running `minishift start`
  ```bash
  cat /etc/resolv.conf 
    # Generated by NetworkManager
    nameserver 8.8.8.8
  ```
  Retrieve env via `minishift oc-env`:
  ```bash
  ./minishift oc-env 
    export PATH="/home/alex/.minishift/cache/oc/v3.11.0/linux:$PATH"
    # Run this command to configure your shell:
    # eval $(minishift oc-env)
    ``
1. Create Dockerfile on Gitlab private project
  ```Dockerfile
  FROM nginx
  COPY html-src /usr/share/nginx/html
  ```
2. Clone and Test locally
  ```bash
  git clone git@vps196185.vps.ovh.ca:alexandre/dockerphp.git
  docker build -t dockerphp .
  ```
3. Create Application on running Openshift
  ```bash
  oc login
  oc new-app http://vps196185.vps.ovh.ca/alexandre/dockerphp.git -l name=dockerphp
  ```
4. Configure credendentials of Git server with a secret in Minishift builds tab and rebuild

5. Create route in service tab

6. Access application

## Devops CI/CD sequence diagram
```mermaid
sequenceDiagram
  participant PostesDev
  participant SCM
  participant IntegrationServer
  participant TestServer
  participant ProdServer
  participant RecipeServer
  loop SCMCron
    IntegrationServer-->>SCM: SCM Monitor
  end
  Note right of PostesDev: Code
  PostesDev->>SCM: SCM commit/push
  SCM->>IntegrationServer: update
  Note right of IntegrationServer: Build
  IntegrationServer-->>PostesDev: Build OK ?
  Note right of PostesDev: Verification of code
  IntegrationServer->>TestServer: update
  Note right of TestServer: Tests
  loop TestsOK
    TestServer-->>PostesDev: Tests OK ?
  end
  IntegrationServer->>SCM: SCM tag
  IntegrationServer->>ProdServer: Deploy
  Note right of ProdServer : Deployments<br>on prod envs
  ProdServer-->>PostesDev: Deployment OK ?
  IntegrationServer->>SCM: SCM tag
  IntegrationServer->>RecipeServer: Recipe 
  Note right of PostesDev : Reports Analysis




  loop SprintIteration
    ProdServer-->>PostesDev: Iterate
  end


```