All the resources of DevOps for Ops training

## Content
    1. [Algo](notes_algo.md)
    2. [Shell](notes_lignes_commande.md)
    3. [Consulting role](notes_role_consultant.md)
    4. [Shell scripting](notes_shell.md)
    5. [Redhat server administration](notes_redhat.md)
    6. [Python](cours_python/)
    7. [Docker](notes_docker.md)
    8. [Devops](cours_devops/)
    9. [Cloud](notes_cloud.md)
    10. [Aws](cours_aws)
    11. [MS Azure]()
    12. [MS Windows Server 2012](notes_winsrv.md)
    13. [MS Powershell](cours_powershell/)
    14. [Git & Gitlab](notes_gitlab.md)
    15. Google Cloud Computing
    16. Openshift
    17. Jenkins
    18. Ansible
    19. Terraform
    20. [Project](notes_project.md)