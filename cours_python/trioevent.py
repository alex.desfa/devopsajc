from threading import Thread, Timer, Lock, RLock, Event
import sys
import random
import time

event1 = Event()
event2 = Event()
event3 = Event()

class Hello(Thread):
	def __init__(self, rand, event, eventNext, id):
		Thread.__init__(self)
		self.liste_hello = ['Bonjour', 'hello', 'Oi', 'Ni hao']
		self.idx = 0
		self.id = id
		self.rand = rand
		self.event = event
		self.eventNext = eventNext

	def run(self):
		while self.idx < len(self.liste_hello):
			self.event.wait()
			self.event.clear()
			sys.stdout.write(self.id + self.liste_hello[self.idx])
			sys.stdout.flush()
			self.idx += 1
			timing = random.randint(1,self.rand)
			time.sleep(timing)
			self.eventNext.set()

hello1 = Hello(4, event1,event2, '1')
hello2 = Hello(8, event2,event3, '2')
hello3 = Hello(6, event3,event1, '3')

hello1.start()
hello2.start()
hello3.start()
event1.set()

hello1.join()
hello2.join()

print("Fini")
