import time
import os

from Pompiers.boardmanager import BoardManager

board = BoardManager(10, 10, 20, 10)

os.system('clear')
runCount = 0
res = False
while res == False:
    res = board.run()
    board.display()
    time.sleep(0.1)
    os.system('clear')
    runCount += 1

print('Game finished ! :)  (%d turns needed)'%(runCount))
