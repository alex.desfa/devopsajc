from random import randint

from pompier import Pompier

class BoardManager:
    DFLT_CHAR = " . "
    POMP_CHAR = " P "
    FIRE_CHAR = " F "
    BUSY_CHAR = " @ "
    ERRO_CHAR = " K "
    DELI_CHAR = "***"

    """
    Classe definissant le plan de jeu 2D

      V
    H ______________
      |    F       |
      |        F   |
      |  P         |
      |       P    |
      |    P       |
      |____________|
    """
    def __init__(self, sizeH, sizeV, nbFeux, nbPompiers):
        self.sizeH = sizeH
        self.sizeV = sizeV
        self.board = []
        self.listeFeux = []
        self.listePompiers = []
        self.runCounter = 0

        # init lists of Pompiers and Feux
        self.initListePompiers(nbPompiers)
        self.initListeFeux(nbFeux)

        # init board
        self.initBoard(nbFeux, nbPompiers)

    def setCell(self, position, valeur="default"):
        # position is a tuple (x,y)
        (x, y) = position

        if valeur == "default":
            char = BoardManager.DFLT_CHAR
        elif valeur == "pompier":
            char = BoardManager.POMP_CHAR
        elif valeur == "fire":
            char = BoardManager.FIRE_CHAR
        elif valeur == "busy":
            char = BoardManager.BUSY_CHAR
        else:
            char = BoardManager.ERRO_CHAR

        self.board[x][y] = "%s" %char

    def movePompier(self, Pompier):
        if len(self.listeFeux) > 0 and self.listeFeux[0] != None:
            # select Fire to address
            if (Pompier.firePos == (-1,-1)):
                for fire in self.listeFeux:
                    bIsAddressed = False
                    for pomp in self.listePompiers:
                        if (pomp.firePos == fire):
                            # fire is adressed
                            bIsAddressed = True
                            break;
                    if (bIsAddressed == False):
                        Pompier.setFire(fire)
                        print("Pompier " + str(Pompier.position) + " will fight " + str(fire) + " !")
                        break

            # move
            if (Pompier.firePos != (-1,-1)):
                #print("GoGo!" + str(Pompier.firePos))
                result = Pompier.move()
                if (result == True): # fire finished
                    i = 0
                    for fire in self.listeFeux:
                        if (Pompier.getFire() == fire):
                            self.listeFeux.pop(i)
                            break
                        i += 1
                    Pompier.setFire((-1,-1))

            return False
        else:
            print('No more FIRE !')
            return True

    def initListePompiers(self, nbPompiers):
        for i in range(nbPompiers):
            while (True):
                x = randint(0, self.sizeH - 1)
                y = randint(0, self.sizeV - 1)
                if (not self.isFire((x,y)) and not self.isPompier((x,y))):
                    break
            pompier = Pompier((x,y))
            self.listePompiers.append(pompier)

    def initListeFeux(self, nbFeux):
        for i in range(nbFeux):
            while (True):
                x = randint(0, self.sizeH - 1)
                y = randint(0, self.sizeV - 1)
                if (not self.isFire((x,y)) and not self.isPompier((x,y))):
                    break
            self.listeFeux.append((x,y))

    def addFeux(self, nbFeux):
        # for feu in self.listeFeux:
        #     (x,y) = feu
        #     i = x-1:
        #     if (not self.isFire((x,y)) and not self.isPompier((x,y))):
        #         break
        #     self.listeFeux.append((x,y))
        pass

    def getNbFeux(self):
        return len(self.listeFeux)

    def getNbPompiers(self):
        return len(self.listePompiers)

    def initBoard(self, nbFeux, nbPompiers):
        # fill board with default char
        for x in range(self.sizeH):
            self.board.append([' . '] * self.sizeV)

        # gen listeFeux
        for f in self.listeFeux:
            #print('Adding fire @' + str(f))
            self.setCell(f, "fire")

        # gen listePompiers
        for p in self.listePompiers:
            self.setCell(p.position, "pompier")

    def displaySimple(self):
        print("**********************")
        for x in range(self.sizeH):
            print(self.board[x])

    def isPompier(self, position):
        for p in self.listePompiers:
            if (p.position == position):
                return True
        return False

    def isFire(self, position):
        for p in self.listeFeux:
            if (p == position):
                return True
        return False

    def display(self):
        # separation line
        line = ""
        for y in range(self.sizeV):
            line += BoardManager.DELI_CHAR
        print(line)

        # update board
        for x in range(self.sizeH):
            for y in range(self.sizeV):
                bPompier = self.isPompier((x,y))
                bFire = self.isFire((x,y))

                if bPompier and bFire:
                    self.setCell((x,y), "busy" )
                elif bPompier:
                    self.setCell((x,y), "pompier" )
                elif bFire:
                    self.setCell((x,y), "fire" )
                else:
                    self.setCell((x,y), "default" )

        # display board
        for x in range(self.sizeH):
            line = ""
            for y in range(self.sizeV):
                line += str(self.board[x][y])
            print(line)

    def run(self):
        result = False
        # add fire
        if (self.runCounter % 7 == 0):
            self.initListeFeux(1)

        # launch Pompiers team
        for p in self.listePompiers:
            #print('Go ' + str(p.position) + str(p.firePos))
            result |= self.movePompier(p)

        self.runCounter += 1
        return result


# if __name__ == "__main__":
#     board = BoardManager(10, 10, 1, 1)
#     board.setCell((3,3), 32)
#     board.display()
