from tkinter import *
import time
import os

from boardmanager import BoardManager

class PompierGui(BoardManager):
    def __init__(self, board_w = 16, board_h = 16, nb_feux = 10, nb_pompiers = 12):
        # init board
        BoardManager.__init__(self, board_w,board_h,nb_feux,nb_pompiers)
        self.maxFeu = (board_w * board_h) - self.getNbPompiers()

        #instance variables
        self.fen = Tk() # singleton
        self.fen.configure(width=800,height=600,bg="white")
        self.fen.resizable(width=False,height=False)
        self.cell_h = self.cell_w = 48 # pixels
        self.board_h = board_h # cells
        self.board_w = board_w # cells
        self.run_counter = 0
        self.win = False
        self.lost = False
        self.launched = False
        self.ecranStr = StringVar("")

        #init graphics
        self.photoWelcome = PhotoImage(file="data/welcome.png")
        self.photoPompier = PhotoImage(file="data/pomp_48.gif")
        self.photoFeu = PhotoImage(file="data/fire_48.gif")
        self.photoBusy = PhotoImage(file="data/busy_48.gif")

        self.photoWin = PhotoImage(file="data/winner.gif")
        self.photoLost = PhotoImage(file="data/gameover.gif")
        # resizeImg(photoWin, 800, 600, board_w*cell_w, board_h*cell_h)

        # Create game screen with top bar
        self.ecranFrame = Frame(self.fen, bg="blue", width=800, height=600)
        self.ecranFrame.pack(expand=1, fill=BOTH)
        self.label = Label(self.ecranFrame, textvariable = self.ecranStr, padx=4, pady=12, bg="blue")
        self.label.pack(side=TOP)
        self.button = Button(self.fen, text="PLAY", padx=10, pady=20)
        self.button.bind('<Button-1>', self.play)
        self.button.pack(side=BOTTOM)

        # init canvas
        self.canvas = Canvas(self.fen, bg="white",
                            width=self.cell_w * self.board_w,
                            height=self.cell_h * self.board_h)
        self.canvas.after(20, self.refreshGUI)

        self.fen.mainloop()

    # Functions
    def resizeImg(self, img, w, h, new_w=16, new_h=16):
        scale_w = w//new_w
        scale_h = h//new_h
        #img = img.zoom(scale_w, scale_h)
        img = img.subsample(scale_w, scale_h)

    def displayImg(self, img, x, y):
        self.canvas.create_image(
            self.cell_h/2 + x * self.cell_h,
            self.cell_w/2 + y * self.cell_w,
            image=img)
        self.canvas.pack()

    def displayBoard(self):
        for x in range(self.cell_h):
            for y in range(self.cell_w):
                bPompier = self.isPompier((x,y))
                bFire = self.isFire((x,y))

                if bPompier and bFire:
                    self.displayImg(self.photoBusy, x,y)
                elif bPompier:
                    self.displayImg(self.photoPompier, x,y)
                elif bFire:
                    self.displayImg(self.photoFeu, x,y)
                else:
                    pass

    def play(self, event):
        self.launched = True
        print("PLAY " + str(self.launched))
        self.button.destroy()

    def refreshGUI(self):

        if (self.launched == False):
            # welcome screen
            self.canvas.delete('all')
            self.canvas.create_image(800/2 + self.board_h/2,
                                600/2 + self.board_w/2,
                                image=self.photoWelcome)
            self.canvas.pack(side=TOP)
        else:
            if (not self.win and not self.lost):
                # run game
                nbFeux = self.getNbFeux()

                self.ecranStr.set("Running turn %d (%d fires / %d max)"%(self.run_counter, nbFeux, self.maxFeu))
                res = self.run()
                self.run_counter += 1

                # display board
                self.canvas.delete('all')
                self.displayBoard()

                # check end of game
                if ( res == True):
                    self.win = True
                if (nbFeux >= self.maxFeu):
                    self.lost = True
            else:
                # end of game
                if (self.win == True):
                    self.ecranStr.set("You win after, %d turns !"%(self.run_counter))

                    # update screen
                    self.canvas.delete('all')
                    self.canvas.create_image(800/2 + self.board_h/2,
                                        600/2 + self.board_w/2,
                                        image=self.photoWin)
                    self.canvas.pack(side=BOTTOM)
                elif (self.lost == True):
                    self.ecranStr.set("You lost after, %d turns !"%(self.run_counter))

                    # update screen
                    self.canvas.delete('all')
                    self.canvas.create_image(800/2 + self.board_h/2,
                                        600/2 + self.board_w/2,
                                        image=self.photoLost)
                    self.canvas.pack(side=BOTTOM)
        # recall
        self.canvas.after(200, self.refreshGUI)


if __name__ == "__main__":
     gui = PompierGui(board_w = 32, board_h = 32, nb_feux = 100, nb_pompiers = 12)
