class Pompier:
    def __init__(self, position, busy=0, fire_position=(-1,-1)):
        self.position = position
        self.busy = busy
        self.fireTime = 5
        self.firePos = fire_position

    def isBusy(self):
        return self.busy != 0

    def setFire(self,fire_position):
        self.firePos = fire_position

    def getFire(self):
        return (self.firePos)

    def move(self, nb_coups=1):
        ### extinction du feu
        if (self.busy > 0):
            if (self.busy >= self.fireTime):
                self.busy = 0
                print("FIRE extinct !")
                return True
            else:
                print("WIP ! (%d/%d)"%(self.busy, self.fireTime))
                self.busy += 1
                return False

        ### calcul le chemin pour atteindre le feux
        if (self.firePos == (-1,-1) or self.firePos == None):
            print("Error on firePos")
            return False
        else:
            (fire_x, fire_y) = self.firePos
        counter = 0
        print("Move pompier from " + str(self.position) + " to " + str(self.firePos))

        (init_x, init_y) = self.position
        (new_x, new_y) = self.position
        if (new_x > fire_x):
            new_x -= 1
        else:
            new_x += 1

        if (new_y > fire_y):
            new_y -= 1
        else:
            new_y += 1

        if fire_x != init_x and counter < nb_coups:
            #print("Moving horizontaly from %d to %d" %(init_x, new_x))
            self.position = (new_x,init_y)
            #counter += 1

        if fire_y != init_y and counter < nb_coups:
            #print("Moving verticaly from %d to %d" %(init_y, init_y))
            self.position = (init_x,new_y)
            #counter += 1
        counter += 1

        if self.position == self.firePos:
            print("Reach FIRE !")
            self.busy += 1
            return False
        else:
            return False
