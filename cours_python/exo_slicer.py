import numpy as np

a = [ 2,5,3,1,5,7,6,8,4,3,5,7,8,6,4]

""" 
# récupérer les trois premiers éléments
troisPremiers = a[0:3]
print(troisPremiers)

# récupérer les éléments du 4e au 9e inclus
print(a[3:9])

#jusqu'à 2 elements avant la fin
print(a[:-2:])

#un élément sur 2
print(a[::2])

#un élément sur trois en sens inverse de l'avant dernier au 4e inclus
print(a[-2:2:-3]) """

a = np.ones((12,))
a[4:6] = 3
a[a<3] = 0
a[a>=3] = 9
print(a)