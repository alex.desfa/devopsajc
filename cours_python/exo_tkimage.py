from tkinter import *
import time

#variables
fen = Tk() # singleton
h = w = 128

photo = PhotoImage(file="pomp.gif")
canvas = Canvas(fen, width=w*5, height=h*5)
canvas.create_image(h/2, w/2, image=photo)
canvas.pack()

counter = 0
def run():
    global counter
    canvas.delete('all')
    canvas.create_image(
        h/2 + counter * h, 
        w/2 + counter * h,
        image=photo)
    
    counter += 1
    if ( counter < 5 ):
        print(counter)
        canvas.after(500, run)

canvas.after(500, run)

fen.mainloop()