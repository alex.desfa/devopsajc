import time
import sys
from threading import Thread, Lock, RLock
import random

lock1 = Lock()
lock2 = Lock()

bol = True

class Hello1(Thread):
    
    def __init__(self, name, lock1i, lock2i):
        Thread.__init__(self)
        self.hellos = ['Hello', 'Bonjour', 'Buongiorno', 'Hola', 'Oi', 'Ni hao', 'Ohayoo']
        self.idx = 0
        self.name = name
        self.lock1 = lock1i
        self.lock2 = lock2i

    def run(self):
        while self.idx < 7:
            with self.lock1 if self.idx % 2 == 0 else self.lock2:
                sys.stdout.write(self.name + " says:" + self.hellos[self.idx] + "\n")
                sys.stdout.flush()
                timing = 0.5 + random.randint(1, 6)            
                time.sleep(timing)
                self.idx += 1

thread1 = Hello1("One", lock1, lock2)
thread2 = Hello1(" Two", lock2, lock1)
#thread3 = Hello("  Three")

thread1.start()
thread2.start()
#thread3.start()

thread1.join()
thread2.join()
#thread3.join()

print("FINI !!")