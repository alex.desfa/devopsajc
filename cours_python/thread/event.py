import time
import sys
from threading import Thread, Lock, Event
import random

ev1 = Event()
ev2 = Event()
ev3 = Event()

class Hello(Thread):
    
    def __init__(self, name, event1):
        Thread.__init__(self)
        self.hellos = ['Hello', 'Bonjour', 'Buongiorno', 'Hola', 'Oi', 'Ni hao', 'Ohayoo']
        self.idx = 0
        self.name = name
        self.ev = event1

    def run(self):
        while self.idx < 7:
            # wait for next available ev
            if self.ev == ev1:
                print("%s : waiting for 1" %(self.name))
            elif self.ev == ev2:
                print("%s : waiting for 2" %(self.name))
            else: #self.ev == ev3
                print("%s : waiting for 3" %(self.name))
            self.ev.wait()
            # release previous ev
            self.ev.clear()

            # act               
            sys.stdout.write(self.name + " says:" + self.hellos[self.idx] + "\n")
            sys.stdout.flush()
            timing = 0.5 + random.randint(1, 6)          
            time.sleep(timing)
            
            # launch next ev
            if self.ev == ev1:
                newev = ev2
            elif self.ev == ev2:
                newev = ev3
            else: #self.ev == ev3
                newev = ev1
            newev.set()
            
            

            self.ev = newev
            self.idx += 1
            

            
            
        

thread1 = Hello("One", ev1, ev2)
thread2 = Hello(" Two", ev2, ev3)
thread3 = Hello("  Three", ev3, ev1)

ev1.set()

thread1.start()
thread2.start()
thread3.start()

thread1.join()
thread2.join()
thread3.join()

print("FINI !!")