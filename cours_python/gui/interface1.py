from tkinter import *

class AjcButton(Button):
    def __init__(self, master, text="", padx="", pady=""):        
        Button.__init__(self, master, text=text, padx=padx, pady=pady)
        self.code = text

    def getValeur(self):
        return self.code

#variables
fenetre = Tk() # singleton
fenetre.configure(width=120,height=180)
fenetre.resizable(width=False,height=False)
ecranStr = StringVar()
ecranStr.set("0.0")

def btnHandler(event):
    oldVal = ecranStr.get()
    if (oldVal == "0.0"): oldVal = ""
    if (len(oldVal) <= 64):
        ecranStr.set( oldVal + str(event.widget.getValeur()))

def valHandler(event):
    operation = ecranStr.get()
    result = 0
    #calculate
    try:
        result = eval(operation)
    except:
        result = "ERROR"
    ecranStr.set(result)

def resetHandler(event):
    ecranStr.set("0.0")


# add screen for results
ecranFrame = Frame(fenetre, bg="white")
ecranFrame.pack(expand=1, fill=BOTH)
label = Label(ecranFrame, textvariable = ecranStr, padx=4, pady=12, bg="white", anchor=W, justify=RIGHT)
label.pack(side=TOP)

# then start GUI organization from bottom
valFrame = Frame(fenetre, bg="black")
valFrame.pack(side=BOTTOM, expand=1, fill=BOTH)
button = Button(valFrame, text="VALIDER", padx=120, pady=30)
button.bind('<Button-1>', valHandler)
button.bind('<Button-2>', resetHandler)
button.bind('<Button-3>', resetHandler)

button.pack(side=BOTTOM, expand=1, fill=BOTH)

allNum = Frame(fenetre, bg="black")
allNum.pack(side=LEFT, expand=1, fill=BOTH)

button0 = AjcButton(allNum, text="0", padx=90, pady=30)
button0.bind('<Button-1>', btnHandler)
button0.pack(side=BOTTOM, expand=1, fill=BOTH)

numbersFrame = Frame(allNum, bg="black")
numbersFrame.pack(side=LEFT, expand=1, fill=BOTH)
pos = [(2,0), (2,1), (2,2), (1,0), (1,1), (1,2), (0,0), (0, 1), (0, 2)]
for i in range(9):
    (x,y) = pos[i]
    button1 = AjcButton(numbersFrame, text="%d"%(i+1), padx=30, pady=30)
    button1.bind('<Button-1>', btnHandler)
    button1.grid(row=x, column=y)

operatorsFrame = Frame(fenetre, bg="black")
operatorsFrame.pack(side=RIGHT, expand=1, fill=BOTH)
for i in ["+", "-", "*", "/"]:
    button = AjcButton(operatorsFrame, text="%s"%i, padx=30, pady=30)
    button.bind('<Button-1>', btnHandler)
    button.pack(side=TOP)


fenetre.mainloop()

print("Done")