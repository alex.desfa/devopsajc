class Pompier:
	def __init__(self, position):
		self.position = position
		self.is_busy = 0
	def se_deplacer_vers_feu(self, feu):
		"""
		Deplace le pompier d'une case vers le feu en param
		"""
		if self.position[0] != feu[0]:
			self.position[0] += 1 if self.position[0] < feu[0] else -1
		elif self.position[1] != feu[1]:
			self.position[1] += 1 if self.position[1] < feu[1] else -1