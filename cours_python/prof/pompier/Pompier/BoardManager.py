from .Pompier import Pompier

class BoardManager:
	def __init__(self):
		self.liste_pompiers = [Pompier([1,3])]
		self.liste_feux = [[6,7]]
	def run(self):
		"""
		Déplace le pompier vers le feu
		"""
		self.liste_pompiers[0].se_deplacer_vers_feu(self.liste_feux[0])
	def is_pompier(self,x,y):
		return self.liste_pompiers[0].position == [x,y]
	def is_feu(self,x,y):
		return self.liste_feux[0] == [x,y]
	def display(self):
		"""
		Affiche les pompiers et les feux
		"""
		print("Pompier : %s"%str(self.liste_pompiers[0].position))
		print("Feu : %s"%str(self.liste_feux[0]))
		for i in range(10):
			line = ""
			for j in range(10):
				if self.is_pompier(i,j):
					line += 'x' 
				elif self.is_feu(i,j):
					line += 'i'
				else:
					line += ' '
			print(line)


