# from MonPackage import sayHi
# from MonPackage import tri_fusion
# sayHi()
# print(tri_fusion([0,32,2,4,8,16,22,-7]))

from BankAccount.Personne import Personne
from BankAccount.Personne import Client
from BankAccount.BankAccount import BankAccount

cliMickey = Client("Mickey", "Mouse", solde=1000000)
cliMinie= Client("Minie", "Mouse", solde=100000)

cliMinie.sayHi(cliMickey)

print(cliMickey.compte.manager)
print(cliMinie.compte.manager)

cliMickey.compte.printInfo()
cliMinie.compte.printInfo()

cliMickey.transfer(cliMinie, 232)

cliMickey.compte.printSolde()
cliMinie.compte.printSolde()

print(cliMickey < cliMinie)

print(cliMickey.banque)