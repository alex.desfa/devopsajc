from .BankAccount import BankAccount

class PersonneInterface:
    def sayHi(self, autre_personne):
        raise NotImplementedError()

class Personne(PersonneInterface):
    def __init__(self, prenom, nom, age=40):
        self.prenom = prenom
        self.nom = nom
        self.age = age
    def sayHi(self, autre_personne):
        print("Bonjour %s, je m'appelle %s %s"% (
            autre_personne.prenom, self.prenom, self.nom))
    def __repr__(self):
        return "Je suis la personne : %s, %s" %(self.prenom, self.nom)

class AgentFinancier:
    def __init__(self, banque):
        self.banque = banque
    def sayHi(self, autre_personne):
        print("Je suis un agent financier")
    def estRaisonnable(self):
        return True

class Client(Personne, AgentFinancier):
    manager = Personne("Balthazar" ,"Picsou")
    def __init__(self, prenom, nom, manager=None, solde=0):
        Personne.__init__(self, prenom, nom)
        AgentFinancier.__init__(self,"Natixis")
        if manager == None:
            manager = Client.manager
        else:
            manager = manager
        self.compte = BankAccount(self, manager, solde)
    
    def transfer(self, autre_personne, valeur):
        self.compte.retrait(valeur)
        autre_personne.compte.credit(valeur)
    
    def __lt__(self, other):
        return self.compte.solde < other.compte.solde
    def __eq__(self, other):
        return self.compte.solde == other.compte.solde
    def __gt__(self, other):
        return self.compte.solde > other.compte.solde



if __name__ == "__main__":
    une_personne = Personne("Toto", "Martin")
    autre_personne = Personne("Jane", "Ma")
    une_personne.sayHi(autre_personne)