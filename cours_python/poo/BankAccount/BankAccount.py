class SoldeInsufisantError(Exception):
    pass

class ValeurNegativeError(Exception):
    pass

class BankAccount:
    """
    Classe définissant un compte bancaire avec un client,
    un solde et un manager
    Il y aura en plus les méthodes :
    retrait et crédit pour retirer et ajouter de l'argent 
    sur le solde du compte
    """
    def __init__(self, client, manager=None, solde = 100):
        self.owner = client
        self.manager = manager
        self.solde = solde

    def retrait(self, montant):
        if montant < 0:
            raise ValeurNegativeError()
        if self.solde < montant:
            raise SoldeInsufisantError()
        self.solde -= montant

    def credit(self, montant):
        if montant < 0:
            raise ValeurNegativeError
        self.solde += montant

    def printInfo(self):
        print("Bonjour M/Mme %s %s,\n votre conseiller est %s %s.\n Votre solde est de %d €"
        %(  self.owner.prenom, self.owner.nom,
            self.manager.prenom, self.manager.nom,
            self.solde))

    def printSolde(self):
        print("M/Mme %s, votre solde est de %d €"%(
            self.owner.nom, self.solde))
