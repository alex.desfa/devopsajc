def _fusion(arrayA, arrayB):
    arrayRes = []
    idxA, idxB = 0,0
    while (idxA < len(arrayA) and idxB < len(arrayB)):
        if arrayA[idxA] < arrayB[idxB]:
            arrayRes.append(arrayA[idxA])
            idxA = idxA + 1
        else:
            arrayRes.append(arrayB[idxB])
            idxB = idxB + 1
            
    arrayRes.extend(arrayA[idxA:])
    arrayRes.extend(arrayB[idxB:])
    return arrayRes

def tri_fusion(array):
    """
    Renvoie le tableau array trié en utilisant l'algo tri fusion
    """
    if len(array) == 1:
        return array
    if len(array) == 2:
        if array[0] > array[1]:
            return array[::-1]
        else:
            return array
    middle = len(array)//2
    return _fusion(
        tri_fusion(array[:middle]), 
        tri_fusion(array[middle:]))