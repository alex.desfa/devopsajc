from flask import Flask, render_template, request, redirect
from models import db
from models.personne import Personne
from models.client import Client

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///ajcdb.sqlite3"

with app.app_context():
    db.init_app(app)
    db.create_all()

# @app.route("/")
# def hello_world():
#     return "HelloOOOOOo world"

# css : bootstrap, materialize, bulma
# themes : https://themeforest.net/ 

# @app.route('/')
# def hello():
#     liste_personnes = Personne.query.all()
#     return render_template('index.html', liste_personnes=liste_personnes)

# @app.route('/new_personne', methods=['POST'])
# def new_personne():
#     name = request.form.get('nom')
#     firstname = request.form.get('prenom')
#     age = request.form.get('age')
#     personne = Personne (name, firstname, age)
#     db.session.add(personne)
#     db.session.commit()

#     return render_template('index.html', personne={
#         'name': name, 'firstname': firstname, 'age':age
#         })
#     #return redirect("/")

@app.route('/')
def hello():
    liste_clients = Client.query.all()
    return render_template('index.html', liste_personnes=liste_clients)

@app.route('/new_personne', methods=['POST'])
def new_personne():
    name = request.form.get('nom')
    firstname = request.form.get('prenom')
    age = request.form.get('age')
    balance = request.form.get('balance')

    client = Client.query.filter_by(name = name, firstname = firstname, age = age).first()    
    if (client != None):
        balance = client.getBalance()
    else:
        personne = Client (name, firstname, age)
        db.session.add(personne)
        db.session.commit()

    return render_template('index.html', personne={
        'name': name, 'firstname': firstname, 'age':age, 'balance': balance
        })
    #return redirect("/")

@app.route('/client_info', methods=['POST'])
def client_info():
    balance = "UNKNOWN"
    name = request.form.get('name')
    firstname = request.form.get('firstname')
    age = request.form.get('age')

    client = Client.query.filter_by(name = name, firstname = firstname, age = age).first()
    print("%s %s %s"%(name, firstname, age))   
    
    if (client != None):
        balance = client.getBalance()
        print("id: %d , balance: %d"%(client.id, balance)) 
    
    return render_template('index.html', personne={
        'name': name, 'firstname': firstname, 'age':age, 'balance': balance
        })
    #return redirect("/")

@app.route('/withdraw', methods=['POST'])
def withdraw():
    name = request.form.get('name')
    firstname = request.form.get('firstname')
    age = request.form.get('age')
    val = request.form.get('withdraw_val')
    print("WITHDRAW: %s" %(val))
    
    client = Client.query.filter_by(name = name, firstname = firstname, age = age).first()
    balance = client.withdraw(int(val))
    db.session.commit()

    return render_template('index.html', personne={
        'name': name, 'firstname': firstname, 'age':age, 'balance':balance
        })

@app.route('/deposit', methods=['POST'])
def deposit():
    name = request.form.get('name')
    firstname = request.form.get('firstname')
    age = request.form.get('age')
    val = request.form.get('deposit_val')
    print("DEPOSIT: %s" %(val))

    client = Client.query.filter_by(name = name, firstname = firstname, age = age).first()
    balance = client.deposit(int(val))
    db.session.commit()

    return render_template('index.html', personne={
        'name': name, 'firstname': firstname, 'age':age, 'balance':balance
        })

if __name__ == '__main__':
    app.run(debug=True, port=8080)