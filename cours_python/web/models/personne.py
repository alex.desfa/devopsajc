from . import db

class Personne(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(100))
    firstname = db.Column(db.String(100))
    age = db.Column(db.String(100))

    def __init__(self, name, firstname, age):
        self.name = name
        self.firstname =firstname
        self.age = age