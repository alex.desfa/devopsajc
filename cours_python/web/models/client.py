from . import db

# choix de base de donnees : theoreme de CAP
# Consistency / Availability / Partition Tolerance
# https://www.mysoftkey.com/architecture/understanding-of-cap-theorem/

class SoldeInsufisantError(Exception):
    pass

class ValeurNegativeError(Exception):
    pass

class Client(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(100))
    firstname = db.Column(db.String(100))
    age = db.Column(db.String(100))
    balance = db.Column(db.Integer)

    def __init__(self, name, firstname, age, balance=32):
        self.name = name
        self.firstname = firstname
        self.age = age
        self.balance = int(balance)

    def withdraw(self, value):
        if value < 0:
            raise ValeurNegativeError()
        if self.balance < value:
            raise SoldeInsufisantError()
        self.balance -= value
        return self.balance

    def deposit(self, value):
        if value < 0:
            raise ValeurNegativeError()
        self.balance += value
        return self.balance

    def getBalance(self):
        return self.balance