# Install Windows 2012 Server

## Configuration
- 1 serveur 2012 avec interface graphique (admin!123456)
    - ALEX-SRV-12 / Ip : 192.168.0.32 / Dns : 127.0.0.1 
- 1 client Windows 10
    - ALEX-CLIENT-10 / 192.168.0.10 / Dns : 192.168.0.32

## Réseau
- 2 machines virtuelles sur la même configuration (bridge)
    - ```ncpa.clp``` panneau de configuration
    - ```sconfig``` outil en ligne de commande sur le serveur
    - ```slmgr -rearm``` pour relancer la periode d'essai client et/ou serveur


## Active Directory
- Installation du rôle AD/DS sur le serveur :
    - nom de forêt / domaine : alexdomain.lan
    - nom NetBIOS : ALEXDOMAIN

- script  Windows PowerShell pour le déploiement d’AD DS
    ```
    Import-Module ADDSDeployment
    Install-ADDSForest `
    -CreateDnsDelegation:$false `
    -DatabasePath "C:\Windows\NTDS" `
    -DomainMode "Win2012R2" `
    -DomainName "alexdomain.lan" `
    -DomainNetbiosName "ALEXDOMAIN" `
    -ForestMode "Win2012R2" `
    -InstallDns:$true `
    -LogPath "C:\Windows\NTDS" `
    -NoRebootOnCompletion:$false `
    -SysvolPath "C:\Windows\SYSVOL" `
    -Force:$true
    ```
- le serveur d'administration n'a alors plus de compte local, mais un compte de domaine, il est Controleur de Domaine

- nouveau compte admin : Boss/Azerty666

# DHCP
# GPO
```gpupdate /force``` appliquer la nouvelle stratégie en se connectant à AD

# Annexes
- USB Rubber Ducky : https://github.com/hak5darren/USB-Rubber-Ducky/wiki/Payloads
- clé USB condensateur
- https://ninite.com/

# Powershell
- Powershell & Powershell ISE
- applets en Action-Objet
- typage implicite des variables :
    ``` $var1 = 1
    PS C:\Users\Boss> $var2 = "1"
    PS C:\Users\Boss> $var1 + $var2
    2
    PS C:\Users\Boss> $var2 + $bar1
    1
    PS C:\Users\Boss> $var2 + $var1
    11
    ```
- listage des erreurs du shell :
    ```
    $error
    $error.count
    $error[0] #contient la dernière erreur
    ```
- comparaisons :
    ```
    -eq -ne -gt ...
    -ceq -cne -cgt ... #sensible à la casse
    ```
- identification des elements par similarité:
    ```
    write-host "Bonjour en jaune" -ForegroundColor yellow
    Bonjour en jaune (OK)
    PS C:\Users\Boss> write-host "Bonjour en jaune" -For ye
    Bonjour en jaune (OK)
    ```
- alias
    ```Get-Alias```
    ```New-Alias```

## Modules
```
Get-Command *module*
($env:PSModulePath).split(";")

Import-Module C:\Windows\System32\WindowsPowerShell\v1.0\Modules\MyModules\trouve_chiffre.psm1
man Trouve
```

## tunel ssh
ssh -D 